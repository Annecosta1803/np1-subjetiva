package br.edu.uniateneu.web.service.NP1.sub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebServiceNp1SubApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebServiceNp1SubApplication.class, args);
	}

}
