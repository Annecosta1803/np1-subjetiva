package br.edu.uniateneu.web.service.NP1.sub.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.edu.uniateneu.web.service.NP1.sub.modelo.Endereco;
import br.edu.uniateneu.web.service.NP1.sub.modelo.ResponseModel;
import br.edu.uniateneu.web.service.NP1.sub.repository.EnderecoRepository;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.Produces;

@RestController
@RequestMapping(value = "/enderecos")
public class EnderecoService {

@Autowired
EnderecoRepository enderecoRepository;

@Consumes("application/json")
@Produces("application/json")
@RequestMapping(value = "/salvar", method = RequestMethod.POST)
public @ResponseBody Endereco salvar(@RequestBody Endereco end) {
	Endereco entity = this.enderecoRepository.save(end);
	return entity;
}

@Produces("application/json")
@RequestMapping(value = "/list", method = RequestMethod.GET)
public @ResponseBody List<Endereco> listar() {
	List<Endereco> entities = this.enderecoRepository.findAll();
	return entities;
}

@Consumes("application/json")
@Produces("application/json")
@RequestMapping(value = "/salvar", method = RequestMethod.PUT)
public @ResponseBody Endereco atualizar(@RequestBody Endereco end) {
	Endereco entity = this.enderecoRepository.save(end);
	return entity;
}

@Consumes("application/json")
@RequestMapping(value = "/deletar/{codigo}", method = RequestMethod.DELETE)
public @ResponseBody ResponseModel excluir(@PathVariable("codigo") Long codigo) {
	try {
		Endereco endereco = enderecoRepository.getReferenceById(codigo);
		System.out.println("********" + endereco.getId());

		if (enderecoRepository.findById(codigo).isEmpty()) {
			return new ResponseModel(500, "Endereco não encontrada.");
		}

		enderecoRepository.delete(endereco);
		return new ResponseModel(200, "Registro excluido com sucesso!");

	} catch (Exception e) {
		return new ResponseModel(500, e.getMessage());
	}
}
}
