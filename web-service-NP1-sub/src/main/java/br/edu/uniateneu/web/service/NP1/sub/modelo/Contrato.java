package br.edu.uniateneu.web.service.NP1.sub.modelo;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name= "tb_contrato")
public class Contrato {
	@Id
	private long id;
	private String desconto;
	private String dtVencimento;
	private String tipo_Contrato;
	private String dt_Contrato;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public String getDesconto() {
		return desconto;
	}
	public void setDesconto(String desconto) {
		this.desconto = desconto;
	}
	public String getDtVencimento() {
		return dtVencimento;
	}
	public void setDtVencimento(String dtVencimento) {
		this.dtVencimento = dtVencimento;
	}
	public String getTipo_Contrato() {
		return tipo_Contrato;
	}
	public void setTipo_Contrato(String tipo_Contrato) {
		this.tipo_Contrato = tipo_Contrato;
	}
	public String getDt_Contrato() {
		return dt_Contrato;
	}
	public void setDt_Contrato(String dt_Contrato) {
		this.dt_Contrato = dt_Contrato;
	}

	
	
}
