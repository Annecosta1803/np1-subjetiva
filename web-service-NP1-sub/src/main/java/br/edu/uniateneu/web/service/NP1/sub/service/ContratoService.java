package br.edu.uniateneu.web.service.NP1.sub.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.edu.uniateneu.web.service.NP1.sub.modelo.Contrato;
import br.edu.uniateneu.web.service.NP1.sub.modelo.ResponseModel;
import br.edu.uniateneu.web.service.NP1.sub.repository.ContratoRepository;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.Produces;

@RestController
@RequestMapping(value = "/contratos")
public class ContratoService {

@Autowired
ContratoRepository contratoRepository;

@Consumes("application/json")
@Produces("application/json")
@RequestMapping(value = "/salvar", method = RequestMethod.POST)
public @ResponseBody Contrato salvar(@RequestBody Contrato end) {
	Contrato entity = this.contratoRepository.save(end);
	return entity;
}

@Produces("application/json")
@RequestMapping(value = "/list", method = RequestMethod.GET)
public @ResponseBody List<Contrato> listar() {
	List<Contrato> entities = this.contratoRepository.findAll();
	return entities;
}

@Consumes("application/json")
@Produces("application/json")
@RequestMapping(value = "/salvar", method = RequestMethod.PUT)
public @ResponseBody Contrato atualizar(@RequestBody Contrato end) {
	Contrato entity = this.contratoRepository.save(end);
	return entity;
}

@Consumes("application/json")
@RequestMapping(value = "/deletar/{codigo}", method = RequestMethod.DELETE)
public @ResponseBody ResponseModel excluir(@PathVariable("codigo") Long codigo) {
	try {
		Contrato contrato = contratoRepository.getReferenceById(codigo);
		System.out.println("********" + contrato.getId());

		if (contratoRepository.findById(codigo).isEmpty()) {
			return new ResponseModel(500, "Contrato não encontrada.");
		}

		contratoRepository.delete(contrato);
		return new ResponseModel(200, "Registro excluido com sucesso!");

	} catch (Exception e) {
		return new ResponseModel(500, e.getMessage());
	}
}
}
