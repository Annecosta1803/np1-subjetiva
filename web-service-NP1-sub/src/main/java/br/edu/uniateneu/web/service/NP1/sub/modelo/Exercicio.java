package br.edu.uniateneu.web.service.NP1.sub.modelo;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "tb_exercicio")
public class Exercicio {
	@Id
	private long id;
	private TipoExercicio tpExercicio;
	// private String tp_Exercicio;
	private String reps;
	private String series;

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public TipoExercicio getTpExercicio() {
		return tpExercicio;
	}

	public void setTpExercicio(TipoExercicio tpExercicio) {
		this.tpExercicio = tpExercicio;
	}


	public String getReps() {
		return reps;
	}

	public void setReps(String reps) {
		this.reps = reps;
	}

	public String getSeries() {
		return series;
	}

	public void setSeries(String series) {
		this.series = series;
	}

}
