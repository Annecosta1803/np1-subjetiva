package br.edu.uniateneu.web.service.NP1.sub.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.edu.uniateneu.web.service.NP1.sub.modelo.Contato;
@Repository
public interface ContatoRepository extends JpaRepository<Contato, Long> {

}