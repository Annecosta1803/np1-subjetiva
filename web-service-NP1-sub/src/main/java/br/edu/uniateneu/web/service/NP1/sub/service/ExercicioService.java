package br.edu.uniateneu.web.service.NP1.sub.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.edu.uniateneu.web.service.NP1.sub.modelo.Exercicio;
import br.edu.uniateneu.web.service.NP1.sub.modelo.ResponseModel;
import br.edu.uniateneu.web.service.NP1.sub.repository.ExercicioRepository;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.Produces;

@RestController
@RequestMapping(value = "/exercicios")
public class ExercicioService {

@Autowired
ExercicioRepository exercicioRepository;

@Consumes("application/json")
@Produces("application/json")
@RequestMapping(value = "/salvar", method = RequestMethod.POST)
public @ResponseBody Exercicio salvar(@RequestBody Exercicio end) {
	Exercicio entity = this.exercicioRepository.save(end);
	return entity;
}

@Produces("application/json")
@RequestMapping(value = "/list", method = RequestMethod.GET)
public @ResponseBody List<Exercicio> listar() {
	List<Exercicio> entities = this.exercicioRepository.findAll();
	return entities;
}

@Consumes("application/json")
@Produces("application/json")
@RequestMapping(value = "/salvar", method = RequestMethod.PUT)
public @ResponseBody Exercicio atualizar(@RequestBody Exercicio end) {
	Exercicio entity = this.exercicioRepository.save(end);
	return entity;
}

@Consumes("application/json")
@RequestMapping(value = "/deletar/{codigo}", method = RequestMethod.DELETE)
public @ResponseBody ResponseModel excluir(@PathVariable("codigo") Long codigo) {
	try {
		Exercicio exercicio = exercicioRepository.getReferenceById(codigo);
		System.out.println("********" + exercicio.getId());

		if (exercicioRepository.findById(codigo).isEmpty()) {
			return new ResponseModel(500, "Exercicio não encontrada.");
		}

		exercicioRepository.delete(exercicio);
		return new ResponseModel(200, "Registro excluido com sucesso!");

	} catch (Exception e) {
		return new ResponseModel(500, e.getMessage());
	}
}
}

