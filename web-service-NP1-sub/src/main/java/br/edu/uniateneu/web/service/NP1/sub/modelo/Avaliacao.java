package br.edu.uniateneu.web.service.NP1.sub.modelo;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name= "tb_avaliacao")
public class Avaliacao {
	@Id
	private long id;
	private String peso;
	private String altura;
	private String biceps;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getPeso() {
		return peso;
	}
	public void setPeso(String peso) {
		this.peso = peso;
	}
	public String getAltura() {
		return altura;
	}
	public void setAltura(String altura) {
		this.altura = altura;
	}
	public String getBiceps() {
		return biceps;
	}
	public void setBiceps(String biceps) {
		this.biceps = biceps;
	}
	
	
}
