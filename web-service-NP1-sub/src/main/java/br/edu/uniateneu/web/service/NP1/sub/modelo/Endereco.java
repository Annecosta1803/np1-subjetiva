package br.edu.uniateneu.web.service.NP1.sub.modelo;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name= "tb_endereco")
public class Endereco {
	@Id
	private long id;
	private String logradouro;
	private String tipo_Logradouro;
	private String numero;
	private String cep;
	private String complemento;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public String getLogradouro() {
		return logradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public String getTipo_Logradouro() {
		return tipo_Logradouro;
	}
	public void setTipo_Logradouro(String tipo_Logradouro) {
		this.tipo_Logradouro = tipo_Logradouro;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	
	

}

