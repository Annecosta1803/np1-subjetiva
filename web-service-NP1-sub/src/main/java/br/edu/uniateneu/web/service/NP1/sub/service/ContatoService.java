package br.edu.uniateneu.web.service.NP1.sub.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.edu.uniateneu.web.service.NP1.sub.modelo.Contato;
import br.edu.uniateneu.web.service.NP1.sub.modelo.ResponseModel;
import br.edu.uniateneu.web.service.NP1.sub.repository.ContatoRepository;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.Produces;

@RestController
@RequestMapping(value = "/contatos")
public class ContatoService {

@Autowired
ContatoRepository contatoRepository;

@Consumes("application/json")
@Produces("application/json")
@RequestMapping(value = "/salvar", method = RequestMethod.POST)
public @ResponseBody Contato salvar(@RequestBody Contato end) {
	Contato entity = this.contatoRepository.save(end);
	return entity;
}

@Produces("application/json")
@RequestMapping(value = "/list", method = RequestMethod.GET)
public @ResponseBody List<Contato> listar() {
	List<Contato> entities = this.contatoRepository.findAll();
	return entities;
}

@Consumes("application/json")
@Produces("application/json")
@RequestMapping(value = "/salvar", method = RequestMethod.PUT)
public @ResponseBody Contato atualizar(@RequestBody Contato end) {
	Contato entity = this.contatoRepository.save(end);
	return entity;
}

@Consumes("application/json")
@RequestMapping(value = "/deletar/{codigo}", method = RequestMethod.DELETE)
public @ResponseBody ResponseModel excluir(@PathVariable("codigo") Long codigo) {
	try {
		Contato contato = contatoRepository.getReferenceById(codigo);
		System.out.println("********" + contato.getId());

		if (contatoRepository.findById(codigo).isEmpty()) {
			return new ResponseModel(500, "Contato não encontrada.");
		}

		contatoRepository.delete(contato);
		return new ResponseModel(200, "Registro excluido com sucesso!");

	} catch (Exception e) {
		return new ResponseModel(500, e.getMessage());
	}
}
}
