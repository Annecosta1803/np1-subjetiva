package br.edu.uniateneu.web.service.NP1.sub.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.edu.uniateneu.web.service.NP1.sub.modelo.Modalidade;
import br.edu.uniateneu.web.service.NP1.sub.modelo.ResponseModel;
import br.edu.uniateneu.web.service.NP1.sub.repository.ModalidadeRepository;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.Produces;

@RestController
@RequestMapping(value = "/modalidades")
public class ModalidadeService {

@Autowired
ModalidadeRepository modalidadeRepository;

@Consumes("application/json")
@Produces("application/json")
@RequestMapping(value = "/salvar", method = RequestMethod.POST)
public @ResponseBody Modalidade salvar(@RequestBody Modalidade end) {
	Modalidade entity = this.modalidadeRepository.save(end);
	return entity;
}

@Produces("application/json")
@RequestMapping(value = "/list", method = RequestMethod.GET)
public @ResponseBody List<Modalidade> listar() {
	List<Modalidade> entities = this.modalidadeRepository.findAll();
	return entities;
}

@Consumes("application/json")
@Produces("application/json")
@RequestMapping(value = "/salvar", method = RequestMethod.PUT)
public @ResponseBody Modalidade atualizar(@RequestBody Modalidade end) {
	Modalidade entity = this.modalidadeRepository.save(end);
	return entity;
}

@Consumes("application/json")
@RequestMapping(value = "/deletar/{codigo}", method = RequestMethod.DELETE)
public @ResponseBody ResponseModel excluir(@PathVariable("codigo") Long codigo) {
	try {
		Modalidade modalidade = modalidadeRepository.getReferenceById(codigo);
		System.out.println("********" + modalidade.getId());

		if (modalidadeRepository.findById(codigo).isEmpty()) {
			return new ResponseModel(500, "Modalidade não encontrada.");
		}

		modalidadeRepository.delete(modalidade);
		return new ResponseModel(200, "Registro excluido com sucesso!");

	} catch (Exception e) {
		return new ResponseModel(500, e.getMessage());
	}
}
}