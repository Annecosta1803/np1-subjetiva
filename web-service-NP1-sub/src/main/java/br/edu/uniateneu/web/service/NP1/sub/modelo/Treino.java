package br.edu.uniateneu.web.service.NP1.sub.modelo;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name= "tb_treino")
public class Treino {
	@Id
	private long id;
	private String dt_treino;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getDt_treino() {
		return dt_treino;
	}

	public void setDt_treino(String dt_treino) {
		this.dt_treino = dt_treino;
	}

}
