package br.edu.uniateneu.web.service.NP1.sub.modelo;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name= "tb_contato")
public class Contato {
	@Id
	private long id;
	private TipoContato tpContato;
	private String valor;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public TipoContato getTpContato() {
		return tpContato;
	}
	public void setTpContato(TipoContato tpContato) {
		this.tpContato = tpContato;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	
	
}
