package br.edu.uniateneu.web.service.NP1.sub.modelo;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name= "tb_aluno")
public class Modalidade {
	@Id
	private long id;
	private TipoModalidade tpModalidade;
	private String valor;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public TipoModalidade getTpModalidade() {
		return tpModalidade;
	}
	public void setTpModalidade(TipoModalidade tpModalidade) {
		this.tpModalidade = tpModalidade;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	
}

