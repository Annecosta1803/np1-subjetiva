package br.edu.uniateneu.web.service.NP1.sub.service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.edu.uniateneu.web.service.NP1.sub.modelo.Aluno;
import br.edu.uniateneu.web.service.NP1.sub.modelo.ResponseModel;
import br.edu.uniateneu.web.service.NP1.sub.repository.AlunoRepository;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.Produces; 

@RestController 
@RequestMapping(value = "/alunos") 
public class AlunoService { 
 @Autowired 
 AlunoRepository alunoRepository; 

 @Consumes("application/json") 
 @Produces("application/json") 
 @RequestMapping(value = "/salvar", method = RequestMethod.POST) 
 public @ResponseBody Aluno salvar(@RequestBody Aluno end) { 
  Aluno entity  = this.alunoRepository.save(end); 
  return entity; 
 } 

 @Produces("application/json") 
 @RequestMapping(value = "/list", method = RequestMethod.GET) 
 public @ResponseBody List<Aluno> listar() { 
  List<Aluno> entities  = this.alunoRepository.findAll(); 
  return entities; 
 } 

 @Consumes("application/json") 
 @Produces("application/json") 
 @RequestMapping(value = "/salvar", method = RequestMethod.PUT) 
 public @ResponseBody Aluno atualizar(@RequestBody Aluno end) { 
  Aluno entity  = this.alunoRepository.save(end); 
  return entity; 
 } 

 @Produces("application/json") 
 @RequestMapping(value = "/deletar/{codigo}", method = RequestMethod.DELETE) 
 public @ResponseBody ResponseModel excluir(@PathVariable("codigo") Long codigo) 
{ 
  try { 
  Aluno aluno = alunoRepository.getReferenceById(codigo); 
  System.out.println("********"+ aluno.getId()); 


  if (alunoRepository.findById(codigo).isEmpty()) { 
   return new ResponseModel(500, "Aluno não encontrado."); 
  } 

   alunoRepository.delete(aluno); 
   return new ResponseModel(200, "Registro excluido com sucesso!"); 

  } catch (Exception e) { 
   return new ResponseModel(500, e.getMessage()); 
  } 

 } 
}
