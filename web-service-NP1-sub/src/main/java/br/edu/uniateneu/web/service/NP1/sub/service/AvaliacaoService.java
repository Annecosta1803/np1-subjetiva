package br.edu.uniateneu.web.service.NP1.sub.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.edu.uniateneu.web.service.NP1.sub.modelo.Avaliacao;
import br.edu.uniateneu.web.service.NP1.sub.modelo.ResponseModel;
import br.edu.uniateneu.web.service.NP1.sub.repository.AvaliacaoRepository;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.Produces;

public class AvaliacaoService {
@Autowired
AvaliacaoRepository avaliacaoRepository;

@Consumes("application/json")
@Produces("application/json")
@RequestMapping(value = "/salvar", method = RequestMethod.POST)
public @ResponseBody Avaliacao salvar(@RequestBody Avaliacao end) {
	Avaliacao entity = this.avaliacaoRepository.save(end);
	return entity;
	}

@Produces("application/json")
@RequestMapping(value = "/list", method = RequestMethod.GET)
public @ResponseBody List<Avaliacao> listar() {
		List<Avaliacao> entities = this.avaliacaoRepository.findAll();
		return entities;
	}

@Consumes("application/json")
@Produces("application/json")
@RequestMapping(value = "/salvar", method = RequestMethod.PUT)
public @ResponseBody Avaliacao atualizar(@RequestBody Avaliacao end) {
		Avaliacao entity = this.avaliacaoRepository.save(end);
		return entity;
	}

@Consumes("application/json")
@RequestMapping(value = "/deletar/{codigo}", method = RequestMethod.DELETE)
public @ResponseBody ResponseModel excluir(@PathVariable("codigo") Long codigo) {
		
		try {
		Avaliacao avaliacao = avaliacaoRepository.getReferenceById(codigo);
		System.out.println("********" + avaliacao.getId());

		if (avaliacaoRepository.findById(codigo).isEmpty()) {
				return new ResponseModel(500, "Avaliacao não encontrada.");
			}

			avaliacaoRepository.delete(avaliacao);
			return new ResponseModel(200, "Registro excluido com sucesso!");

		} catch (Exception e) {
				return new ResponseModel(500, e.getMessage());
		}
	}
	
}
